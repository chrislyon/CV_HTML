##
## MAKEFILE
##

## Normalement make all genere les pages et publie le site
## faire un make clean AVANT de push sur repos

BASE_DIR=$(shell pwd)
B_DIR=$(BASE_DIR)


.PHONY:
all:	clean footer main exp about comp download contact form tcomp \
		tab_real realisations recherche tcodacademy \
		publications \
		mentions_legales \
		recruteur \
		publish

#retrait de printable de all

.PHONY: test
test:
	python -m SimpleHTTPServer 8000

.PHONY: clean
clean:
	rm -f *.html
	rm -f $(FOOTER_OBJ)
	rm -f gnr_pdf/*.pdf
	rm -f publish

## -----------------
## Page de BAse 
## -----------------
BASE_JADE=base.pug
BASE_OBJ=base.html

.PHONY: base
base: footer
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(BASE_JADE) >$(BASE_OBJ)

## --------------
## Publication
## --------------
.PHONY: publish
publish:
	./gnr_publish.sh
	chmod +x publish
	./publish

## ----------------------------------
## Footer (include)
## Calcul de la date et l'heure
## ----------------------------------

BASE_FOOTER=include/base_footer.pug
FOOTER_OBJ=include/footer.pug
TS=$(shell date "+%x %X")
YEAR=$(shell date "+%Y")
.PHONY: footer
footer:
	cat $(BASE_FOOTER) \
		| sed -e 's!ANNEE_COURANTE!$(YEAR)!' \
		| sed -e 's!DATE_JOUR_HEURE!$(TS)!' > $(FOOTER_OBJ)

###
### AUTRES PAGES
###

### === MAIN / INDEX ====
MAIN_JADE=main.pug
MAIN_OBJ=main.html

.PHONY: main
main: footer
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(MAIN_JADE) >$(MAIN_OBJ)
	ln main.html index.html

### === EXPERIENCES ====
EXP_JADE=exp.pug
EXP_OBJ=exp.html

.PHONY: exp
exp: footer
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(EXP_JADE) >$(EXP_OBJ)

### === ABOUT ====
ABOUT_JADE=about.pug
ABOUT_OBJ=about.html

.PHONY: about
about: footer
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(ABOUT_JADE) >$(ABOUT_OBJ)

### === PUBLICATIONS ====
PUB_JADE=publications.pug
PUB_OBJ=publications.html

.PHONY: publications
publications: footer
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(PUB_JADE) >$(PUB_OBJ)

### === COMPETENCES ====
COMP_JADE=competences.pug
COMP_OBJ=competences.html

.PHONY: comp
comp: footer tcomp
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(COMP_JADE) >$(COMP_OBJ)

### === FORMATIONS ====
FORM_JADE=formations.pug
FORM_OBJ=formations.html

.PHONY: form
form: footer tcodacademy
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(FORM_JADE) >$(FORM_OBJ)

## ==== Table formations codacademy ====
TABLE_CODACADEMY=include/table_codacademy.pug

.PHONY: tcodacademy
tcodacademy:
	rm -f ${TABLE_CODACADEMY}
	python gnr_codacademy.py > ${TABLE_CODACADEMY}

### === DOWNLOAD ====
DWNLD_JADE=download.pug
DWNLD_OBJ=download.html

.PHONY: download
download: footer
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(DWNLD_JADE) >$(DWNLD_OBJ)

### === CONTACT ====
CONTACT_JADE=contact.pug
CONTACT_OBJ=contact.html

.PHONY: contact
contact: footer
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(CONTACT_JADE) >$(CONTACT_OBJ)

### === Table des competences ===
TABLE_COMP=include/table_competences.pug

.PHONY: tcomp
tcomp:
	rm -f ${TABLE_COMP}
	python gnr_comp.py > $(TABLE_COMP)

### === PRINTABLE ====
PRINT_JADE=printable.pug
PRINT_OBJ=printable.html

.PHONY: printable
printable: footer
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(PRINT_JADE) >$(PRINT_OBJ)

### === tableau des realisations ====
TABLE_REAL=include/table_realisations.pug

.PHONY: tab_real
tab_real:
	rm -f ${TABLE_REAL}
	python gnr_real.py > $(TABLE_REAL)

### === REALISATIONS ====
REAL_JADE=realisations.pug
REAL_OBJ=realisations.html

.PHONY: realisations
realisations: tab_real footer
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(REAL_JADE) >$(REAL_OBJ)

### === MENTIONS LEGALES ====
MLEG_JADE=mentions_legales.pug
MLEG_OBJ=mentions_legales.html

.PHONY: mentions_legales
mentions_legales:
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(MLEG_JADE) >$(MLEG_OBJ)

### === RECHERCHE ====
RECH_JADE=recherche.pug
RECH_OBJ=recherche.html

.PHONY: recherche
recherche:
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(RECH_JADE) >$(RECH_OBJ)

### === RECRUTEUR ====
RECR_JADE=recruteur.pug
RECR_OBJ=recruteur.html

.PHONY: recruteur
recruteur:
	pug --pretty -O "{basedir:'$(B_DIR)'}"  <$(RECR_JADE) >$(RECR_OBJ)

