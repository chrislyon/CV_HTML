#!  /usr/local/bin/python3.7

import pdb

def entete():
    r = """
//
// Table des competences
//
div(class="container")
    div(class="row")
        table(class="table table-bordered ")
            tbody
    """
    return r

def saut_de_page():
    r = """
        table(class="table table-bordered " style="page-break-before: always")
            tbody
    """
    return r

def body():
    FICHIER="textes/competence.txt"

    r = ""

    with open(FICHIER) as f:
        for l in f.readlines():
            l = l.rstrip()
            if l.startswith('\t'):
                l += " #"
                ALL = l.split()
                comp = ALL[0]
                level = ALL[1]+'_star.pug'
                c = int(level[0])
                if c == 1:
                    couleur='(class="bg-light text-black")'
                elif c == 2:
                    couleur='(class="bg-light text-black")'
                elif c == 3:
                    couleur='(class="bg-warning text-black")'
                elif c == 4:
                    couleur='(class="bg-success text-white")'
                elif c == 5:
                    couleur='(class="bg-danger text-white")'
                else:
                    couleur='(class="bg-light text-black")'
                nlevel = "%s/5" % c
                comment = " ".join(ALL[2:-1])
                if comment.strip() == '#':
                    comment = '&nbsp'
                r += """
                tr
                    td &nbsp
                    td%s %s
                    td %s 
                    td
                        div(class="d-inline")
                            include /include/%s
                    td %s
                """ % ( couleur, comp, nlevel, level, comment)
            elif l == "<FF>":
                r += saut_de_page()
            elif l == "<PDF_FF>":
                pass
            else:
                r += """
                tr
                    td(colspan=5 class="bg-info text-white")
                        h5 %s
                """ % l
    return r



def run():
    print( entete() )
    print(body())



if __name__ == '__main__':
    run()
