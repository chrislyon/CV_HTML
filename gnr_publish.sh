# !/bin/bash
## Generation du fichier publish

set -x

## ON recupere les valeurs de user/mdp
. ./parametres

g_publish()
{
	cat publish.base | \
		sed -e "s/%%USER%%/${1}/" | \
		sed -e "s/%%PASSWORD%%/${2}/" \
		> publish

}

## -----------------------------------------
# A changer en fonction de l'environnement
## -----------------------------------------
ENV="PROD"
#ENV="DEV"

if [ "$ENV" = "PROD" ]
then
	g_publish $PROD_USER $PROD_PASS
else
	g_publish $TEST_USER $TEST_PASS
fi
