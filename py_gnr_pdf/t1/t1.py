registerFont(TTFont('Own_Font', os.path.dirname(os.path.abspath(__file__)) + '\\OwnSans-Regular.ttf'))
registerFont(TTFont('OwnBold_Font', os.path.dirname(os.path.abspath(__file__)) + '\\OwnSans-Bold.ttf'))

registerFontFamily("Own_Font",normal="Own_Font",bold="OwnrBold_Font",italic="Own_Font",boldItalic="OwnrBold_Font")

# define parameter for the page and paragraph font
PAGE_WIDTH, PAGE_HEIGHT = landscape(A4)
STYLES                  = getSampleStyleSheet()
STYLES.add( ParagraphStyle(name='Text', fontName = 'Own_Font', fontSize = 10 ))

STYLES.add( ParagraphStyle(name='Centered', fontName = 'Own_Font', fontSize = 10, alignment=TA_CENTER ))
STYLES.add( ParagraphStyle(name='CenteredBig', parent=STYLES['Centered'], fontSize=18, spaceAfter=10) )
STYLES.add( ParagraphStyle(name='CenteredMedium', parent=STYLES['Centered'], fontSize=15, spaceAfter=10) )
