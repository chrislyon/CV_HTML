import io
import sys, os
from reportlab.lib import colors
from reportlab.lib.units import mm, cm
from reportlab.lib.pagesizes import A4, landscape
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.platypus import Paragraph, PageBreak, Spacer
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Image, Spacer

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

Page_Largeur, Page_Hauteur = A4

import re

import datetime
import locale
locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

s = getSampleStyleSheet()
s = s["BodyText"]
s.wordWrap = 'CJK'

## Fontes 
#BASE = 'dejavu-fonts-ttf-2.37/ttf/'
#pdfmetrics.registerFont(TTFont('DejaVu_Sans', BASE+'DejaVuSans.ttf'))
#pdfmetrics.registerFont(TTFont('DejaVu_Sans_Bold', BASE+'DejaVuSans-Bold.ttf'))
#pdfmetrics.registerFont(TTFont('DejaVu_Sans_Italic', BASE+'DejaVuSans-Bold.ttf'))

#pdfmetrics.registerFontFamily("DejaVu_Sans",normal="DejaVu_Sans",bold="DejaVu_Sans",italic="DejaVu_Sans",boldItalic="DejaVu_Sans")

BASE='fontes/Arimo/'
pdfmetrics.registerFont(TTFont('Arimo', BASE+'Arimo-Regular.ttf'))
pdfmetrics.registerFont(TTFont('Arimo_Bold', BASE+'Arimo-Bold.ttf'))
pdfmetrics.registerFont(TTFont('Arimo_Italic', BASE+'Arimo-Italic.ttf'))
pdfmetrics.registerFont(TTFont('Arimo_BoldItalic', BASE+'Arimo-BoldItalic.ttf'))
pdfmetrics.registerFontFamily("Arimo",normal="Arimo",bold="Arimo_Bold",italic="Arimo_Italic",boldItalic="Arimo_BoldItalic")


#NAME = "CV.pdf"
AUTHOR = "Christophe BONNET"
TITLE = "CV CHRISTOPHE BONNET"
SUBJECT = "CV CHRISTOPHE BONNET"
KEYWORDS = ["CV", "CHRISTOPHE BONNET", "INGENIEUR SYSTEME", "LINUX", "DBA", "SCRIPTS", "PYTHON", "SHELL" ]
CREATOR = "Scripts python fait maison"

def from_dob_to_age(born):
    today = datetime.date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

def set_doc(fichier):
    doc = SimpleDocTemplate(fichier, pagesize=A4, rightMargin=2*cm, leftMargin=2*cm, topMargin=2*cm,bottomMargin=2*cm)
    return doc

## Une fonction commune a toute les pages
def MesPages(canvas, doc):
    ts = datetime.datetime.now().strftime("%d/%m/%Y a %X")
    canvas.saveState()

    # Affichage du haut de page
    canvas.setFont('Times-Roman',8)
    canvas.drawString(1*cm, Page_Hauteur-1*cm, "CV CHRISTOPHE BONNET")
    canvas.drawString(15*cm, Page_Hauteur-1*cm, "Généré le %s" % ts)

    # Affichage des numeros de pages
    canvas.setFont('Times-Roman',8)
    canvas.drawString(1*cm, 1*cm, "Page N° %d " % doc.page)

    ## Mise en place des propriétés
    canvas.setAuthor(AUTHOR)
    canvas.setTitle(TITLE)
    canvas.setSubject(SUBJECT)
    canvas.setCreator(CREATOR)
    canvas.setKeywords(KEYWORDS)

    canvas.restoreState()

## ------------------------------
## Generation de l'entete du CV
## ------------------------------
def Entete():
    e = []

    #big_style = ParagraphStyle( name='BOLD', fontName='Times-Roman', fontSize=14,)
    big_style = ParagraphStyle( name='BOLD', fontName='Arimo', fontSize=14,)
    zaf_style = ParagraphStyle( name='BOLD', fontName='ZapfDingbats', fontSize=14,)

    NOM = Paragraph("CHRISTOPHE BONNET", style=big_style)

    TEL = Paragraph(chr(9742), style=zaf_style)
    MAIL = Paragraph(chr(9993), style=zaf_style)
    SITE = Paragraph(chr(9758), style=zaf_style)

    AGE = from_dob_to_age( datetime.date(1965,3,28))

    coord = [ 
            [ NOM ,                     "",     TEL,    " 06 04 52 13 87" ],
            ["550 Route de Lagnieu",    "",     MAIL,   " cb@cv.chrislyon.fr"],
            ["01150 Blyes",             "",     SITE,   " https://cv.chrislyon.fr"],
            ["%d ans" % AGE,                   "",     "",     "" ]
            ]

    style = TableStyle([ 
               ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
               ('INNERGRID', (0,0), (-1,-1), 0.25, colors.white),
               ('BOX', (0,0), (-1,-1), 0.25, colors.white),
                       ])

    e=Table(coord, colWidths=[ 6*cm, 6*cm, 1*cm, 6*cm])
    e.setStyle(style)
    return e

## -----------------
## Titre principal
## -----------------
def Titre():
    t = []
    #TA_LEFT, TA_CENTER or TA_CENTRE, TA_RIGHT and TA_JUSTIFY, with values of 0, 1, 2 and 4
    #title_style = ParagraphStyle( name='TITRE', fontName='Times-Roman', fontSize=16, alignment=1)
    title_style = ParagraphStyle( name='TITRE', fontName='Arimo', fontSize=16, alignment=1, leading=24)
    t.append( Spacer(1, 2*cm) )
    t.append(Paragraph("Ingénieur Système UNIX , Linux, Certifié AIX", style=title_style))
    t.append(Paragraph("Administrateur Base de données Oracle", style=title_style))
    t.append(Paragraph("Développeur multi langage", style=title_style))
    return t

##
## Entete de chapitre
##
def Chapitre( nom ):
    c = []
    chap_style = ParagraphStyle( name='CHAPITRE', fontName='Arimo', fontSize=16 , 
                                    leftIndent = -1.5*cm , 
                                    spaceBefore = 1*cm,
                                    spaceAfter = 1*cm,
                                    )
    c.append(Paragraph('<u>'+nom+'</u>', style=chap_style))
    return c

##
## Génération du chapitre Expérience
##
def Experiences( fichier ):
    h_style = ParagraphStyle( name='head_exp', fontName='Arimo', fontSize=12 , 
                                leftIndent = -0.5*cm, 
                                spaceBefore = 0.5*cm, 
                                spaceAfter = 0.5*cm,
                                alignment = 0,
                                )
    c_style = ParagraphStyle( name='line_exp', fontName='Arimo', fontSize=10 , 
                                leftIndent = 0*cm, 
                                spaceBefore = 10 ,
                                alignment = 0,
                                )
    e = []
    with open(fichier) as f:
        for l in f.readlines():
            h = l[0:2]
            c = l[2:]
            if h == "h3":
                e.append( Paragraph('<b>'+c+'</b>', style=h_style))
            elif h == "li":
                BULL=u"\u2022"
                #BULL=u"\u29BE" #affiche un carré noir
                #BULL=u"\u25E6" #affiche un carré noir 
                #BULL=chr(9675) # idem
                e.append( Paragraph(c, style=c_style, bulletText=BULL))
    return e

def Comp_Explications():
    data = [ 
            [ "Niveau",         "", "Description du niveau" ],
            [ "Notions",        "1/5", "Connaissances des principes de bases, mais pas de mise en exploitation ou en production" ],
            [ "Utilisateur",    "2/5", "Technologie testée et utilisée, pas ou très peu de mise en exploitation" ],
            [ "Confirmé",       "3/5", "Technologie avec maitrise des fondamentaux de l'administration, mise en oeuvre en production" ],
            [ "Maîtrise",       "4/5", "Technologie maîtrisée depuis de nombreuses années" ],
            [ "Expert",         "5/5", "Reconnu par les autres comme tel" ],
            ]

    style = TableStyle([ 
                        #('BACKGROUND',(0,0),(-1,0),colors.grey),
                        ('FONTNAME', (0,0), (-1,-1), 'Arimo'),
                        ('FONTNAME', (0,0), (-1,0), 'Arimo_Bold'),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                       ])

    t = Table(data)
    t.setStyle(style)
    l_style = ParagraphStyle( name='line_comp', fontName='Arimo', fontSize=10 , 
                                leftIndent = -1*cm, 
                                spaceBefore = 10 , 
                                spaceAfter = 10 , 
                                alignment = 0,
                                )
    l = Paragraph("Sur ces pages vous trouverez le détail de mes compétences et pour ce faire j'ai utilisé la notification suivante :", style=l_style)
    return [ l, t ]

## ------------------------------------
## Génération des tables de compétences
## ------------------------------------
def Competences(fichier):
    style = TableStyle([ 
                        #('BACKGROUND',(0,0),(1,0),colors.grey),
                        ('SPAN', (0,0), (1,0)),
                        ('FONTNAME', (0,0), (-1,-1), 'Arimo'),
                        ('FONTNAME', (0,0), (-1,0), 'Arimo_Bold'),
                        ('VALIGN',(0,0),(-1,-1),'TOP'),
                        ## Nouvelle méthode
                        ('INNERGRID', (0,1), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (1,0), 0.25, colors.black),
                        ('BOX', (0,1), (-1,-1), 0.25, colors.black),
                        # Ancienne Méthode
                        #('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        #('BOX', (0,0), (-1,-1), 0.25, colors.black),
                       ])
    d_style = ParagraphStyle( name='desc_style', fontName='Arimo', fontSize=10 , alignment = 0,)
    with open(fichier) as f:
        c = []
        t = []
        for l in f.readlines():
            if l.startswith('\t'):
                #ligne
                _, label, niveau, desc = re.split(r'\t+', l)
                #print(" Ligne : l=%s n=%s d=%s" % (label, niveau, desc))
                
                desc = desc.replace("<br/>", "\n")
                desc = desc.replace("&#x1F609;", " :) ")
                desc = desc.replace('<a href="/publications.html">', "")
                desc = desc.replace('</a>', "")
                t.append( [ label, niveau+"/5", desc] )
            elif l == '<FF>\n':
                pass
            elif l == '<PDF_FF>\n':
                c.append(PageBreak())
            else:
                # Titre
                if t : 
                    # Genere Table
                    tt = Table(t, colWidths=[ 3.5*cm, 1*cm, 15.5*cm])
                    tt.setStyle(style)
                    c.append(tt)
                    c.append(Spacer(width=0, height=0.5*cm))
                # Puis on recommence
                t = [[ l, "", "" ]]
        ## Le dernier 
        if t:
            # Genere Table
            tt = Table(t, colWidths=[ 3*cm, 1*cm, 16*cm])
            tt.setStyle(style)
            c.append(tt)
    return c

## -------------
## Formations
## -------------
def Formations(fichier):
    f = []
    style = TableStyle([ 
                        #('BACKGROUND',(0,0),(-1,0),colors.grey),
                        #('SPAN', (0,0), (1,0)),
                        ('FONTNAME', (0,0), (-1,-1), 'Arimo'),
                        ('FONTNAME', (0,0), (-1,0), 'Arimo_Bold'),
                        ('VALIGN',(0,0),(-1,-1),'TOP'),
                        ## New
                        ('INNERGRID', (0,1), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (1,0), 0.25, colors.black),
                        ('BOX', (0,1), (-1,-1), 0.25, colors.black),
                        # Old
                        #('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        #('BOX', (0,0), (-1,-1), 0.25, colors.black),
                       ])
    with open(fichier) as fic:
        f = []
        t = []
        for l in fic.readlines():
            if l.startswith('\t'):
                #ligne
                data = re.split(r'\t+', l)
                #print(" Ligne : d=%s" % data )
                t.append( [ data[1], data[2] ] )
            else:
                # Titre
                if t : 
                    # Genere Table
                    tt = Table(t, colWidths=[ 3*cm, 12*cm])
                    tt.setStyle(style)
                    f.append(tt)
                    f.append(Spacer(width=0, height=0.5*cm))
                # Puis on recommence
                t = [[ l, "" ]]
        ## Le dernier 
        if t:
            tt = Table(t, colWidths=[ 3*cm, 12*cm])
            tt.setStyle(style)
            f.append(tt)
            f.append(Spacer(width=0, height=0.5*cm))

    return f

def Publications():
    l_style = ParagraphStyle( name='line_comp', fontName='Arimo', fontSize=12 , 
                                leftIndent = -1*cm, 
                                spaceBefore = 10 , 
                                spaceAfter = 10 , 
                                alignment = 0,
                                )
    l = []
    l.append( Paragraph("Auteur du livre : Scripting Python sous Linux : Développez vos outils système", style=l_style) )
    l.append( Paragraph("Paru le 10 Juin 2020 aux Editions ENI", style=l_style) )
    livre = Image("../images/EISCRYPT_lkn_552x288-publication.jpg")
    livre.drawWidth = 11.68*cm
    livre.drawHeight = 6.10*cm
    livre.hAlign = 'CENTER'
    l.append( livre )
    l.append( Paragraph("2ème Edition paru en juillet 2023 : Noté 5/5 par 2 avis", style=l_style) )
    l.append( Spacer(1, 2*cm) )
    l.append( Paragraph("Publié en Espagnol au mois de Juillet 2021", style=l_style) )
    livre_en = Image("../images/scripting-python-en-linux-desarrolle-sus-herramientas-de-sistema-9782409031526_XL.jpg")
    livre_en.drawWidth = 8.9*cm
    livre_en.drawHeight = 10.8*cm
    livre_en.hAlign = 'CENTER'
    l.append( livre_en )
    return l

## =========
## MAIN 
## =========
def gnr_cv(fichier):
    doc = set_doc(fichier)

    elements = []

    ## Entete
    elements.append( Entete() )
    elements.extend( Titre() )
    ## Experiences
    elements.extend( Chapitre("Expériences") )
    elements.extend( Experiences( "../textes/experiences.txt") )
    elements.append( PageBreak())
    ## Competences
    elements.extend( Comp_Explications() )
    elements.extend( Chapitre("Compétences") )
    elements.extend( Competences("../textes/competence.txt") )
    elements.append( PageBreak())
    ## Formations
    elements.extend( Chapitre("Formations") )
    elements.extend( Formations("../textes/formations.txt") )
    elements.append( PageBreak())
    ## Publications
    elements.extend( Chapitre("Publications") )
    elements.extend( Publications() )
    elements.append( PageBreak())

    # On construit le document 
    doc.build(elements, onFirstPage=MesPages, onLaterPages=MesPages)

if __name__ == "__main__":

    fichier = "CV2.pdf"
    fichier = io.BytesIO()
    gnr_cv(fichier)
    with os.fdopen(sys.stdout.fileno(), "wb", closefd=False) as stdout:
        stdout.write(fichier.getvalue())
        stdout.flush()

    fichier.close()
