from reportlab.lib import colors
from reportlab.lib.units import mm, cm
from reportlab.lib.pagesizes import A4, landscape
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle

Page_Largeur, Page_Hauteur = A4

Titre = "Une table avec ReportLab"

import datetime
import locale
locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

ts = datetime.datetime.now().strftime("%x a %X")

def set_doc():
    doc = SimpleDocTemplate("t10.pdf", 
				pagesize=A4,
				rightMargin=2*cm,
				leftMargin=2*cm,
				topMargin=2*cm,bottomMargin=2*cm)
    return doc

## Une fonction commune a toute les pages
def MesPages(canvas, doc):
    canvas.saveState()

    # Affichage du titre
    canvas.setFont('Times-Roman',14)
    canvas.drawCentredString(Page_Largeur/2, 
				Page_Hauteur-1*cm, Titre)
    canvas.drawCentredString(Page_Largeur/2, 
				Page_Hauteur-1*cm, "_"*len(Titre))

    # Affichage des numeros de pages
    canvas.setFont('Times-Roman',9)
    canvas.drawString(1*cm, 1*cm, "Page N° %d " % doc.page)
    canvas.drawString(15*cm, 1*cm, "Edite le %s" % ts)

    canvas.restoreState()
  
def table():
    t = []

    data = [
            [ "Titre long pour le sujet" , '', '' ],
            [ "Colonne1", "Cooolonnnnneee 2", "Col3" ],
            [ "Colonne1", "Cooolonnnnneee 2", "Col3" ],
            [ "Colonne1", "Cooolonnnnneee 2", "Col3" ],
            [ "Colonne1", "Cooolonnnnneee 2", "Col3" ],
            [ "Colonne1", "Cooolonnnnneee 2", "Col3" ],
            ]

    style = TableStyle([ 
               #('BACKGROUND',(0,0),(1,0),colors.grey),
               ('VALIGN',(0,0),(-1,-1),'TOP'),
               ('INNERGRID', (0,1), (-1,-1), 0.25, colors.black),
               ('BOX', (0,0), (1,0), 0.25, colors.black),
               ('BOX', (0,1), (-1,-1), 0.25, colors.black),
                       ])

    t=Table(data)
    t.setStyle(style)
    return t

def run():
    doc = set_doc()

    elements = []
      
    #On ajoute la table dans les elements
    elements.append(table())

    # On construit le document 
    doc.build(elements, onFirstPage=MesPages,
				 onLaterPages=MesPages)

if __name__ == "__main__":
    run()
