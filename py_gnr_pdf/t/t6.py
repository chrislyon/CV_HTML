 
 
# coding: utf-8  
"""Considérez la séquence code suivant qui fournit un exemple
 très simple « bonjour monde » pour Platypus.
"""
 
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.platypus import BaseDocTemplate, Frame, PageTemplate, NextPageTemplate, Paragraph, PageBreak, Table, \
    TableStyle,SimpleDocTemplate,Spacer
 
from reportlab.lib.units import inch, cm
from reportlab.lib.pagesizes import letter, A4

PAGE_HEIGHT=defaultPageSize[1]; PAGE_WIDTH=defaultPageSize[0]
styles = getSampleStyleSheet()
 
"""Nous importons d'abord quelques constructeurs, quelques styles 
de paragraphes et d'autres commodités d'autres modules.
""" 

from reportlab.lib import colors

from faker import Faker
 
def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.setFont('Helvetica-Bold',24)
    canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-108, Title)
 
    # titre positioner au centre du document 
    # PAGE LARGEUR =PAGE_WIDTH  PAGE HAUTEUR : PAGE_HEIGHT 
 
    canvas.setFont('Times-Roman',9)
    canvas.drawString(inch, 0.75 * inch, "First Page / %s" % pageinfo)
    canvas.restoreState()
"""Nous définissons les caractéristiques fixes de la première page
 du document avec la fonction ci-dessus.
"""
 
def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Roman',9)
    #canvas.drawString(inch, 0.75 * inch, "Page %d %s" % (doc.page, pageinfo))
    canvas.drawString(1*cm, 1*cm, "Page %d %s" % (doc.page, pageinfo))
    canvas.restoreState()
 
 
"""  Puisque nous voulons que les pages après 
la première soient différentes de la première, 
nous définissons une autre mise en page
caractéristiques des autres pages.
 Notez que les deux fonctions ci-dessus utilisent
  les opérations de canevas de niveau pdfgen
peindre les annotations pour les pages."""

def go_par():
    for i in range(100):
        bogustext = ("N° de paragraphe %s. " % i) *20
        p = Paragraph(bogustext, style)     
        Story.append(p)
        Story.append(Spacer(1,0.2*inch))

def go_table():
    fake = Faker('fr_FR')
    data = []
    # Entete
    row = []
    row.append("Nom")
    row.append("Adresse")
    data.append(row)

    for i in range(200):
        row = []
        row.append( fake.name() )
        row.append( fake.address() )
        data.append( row )

    t = Table(data)
    t.setStyle(TableStyle([
                            #('ALIGN',(1,1),(-2,-2),'RIGHT'),
                            ('BACKGROUND',(0,0),(-1,0),colors.lightgrey),   # Header
                            ('VALIGN',(0,0),(0,-1),'TOP'),
                            #('TEXTCOLOR',(0,0),(0,-1),colors.blue),
                            #('ALIGN',(0,-1),(-1,-1),'CENTER'),
                            #('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                            #('TEXTCOLOR',(0,-1),(-1,-1),colors.green),
                            ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                            ]))
    Story.append(t)


def set_doc():
    doc = SimpleDocTemplate(
            filename="fname.pdf",
            #pagesize=defaultPageSize,
            pagesize=A4,
            #pageTemplates=[],
            showBoundary=0,
            leftMargin=1*cm,
            rightMargin=1*cm,
            topMargin=1*cm,
            bottomMargin=1*cm,
            #leftMargin=inch,
            #rightMargin=inch,
            #topMargin=inch,
            #bottomMargin=inch,
            allowSplitting=1,
            title="Titre du PDF",
            author="Chris",
            _pageBreakQuick=1,
            encrypt=None )
    return doc

 
def go():
    #go_par()
    go_table()
    doc.build(Story, onFirstPage=myFirstPage, onLaterPages=myLaterPages)
 
"""Enfin, nous créons une histoire et construisons le document.
 Notez que nous utilisons ici un modèle de document «en conserve» 
 qui est pré-construit avec des modèles de page. Nous utilisons 
 également un style de paragraphe prédéfini. Nous n'utilisons ici 
 que deux types de fluides: les espaceurs et les paragraphes. 
 Le premier Spacer s'assure que les paragraphes sautent après
la chaîne de titre.
Pour voir la sortie de cet exemple de programme, exécutez 
le module docs / userguide / examples.py (à partir de 
la distribution de docs ReportLab) en tant que "script 
de niveau supérieur". L'interpréteur de script python 
examples.py va générer la sortie Platypus phello.pdf.
"""
if __name__ == '__main__':
    Title = "Hello world"
    pageinfo = "platypus exemple"
    #doc = SimpleDocTemplate("phello.pdf", pagesize=A4)
    doc = set_doc()
    Story =[]
    #Story = [Spacer(1,2*inch)] # sans interret
    Story.append(PageBreak())
    style = styles["Normal"]
 
    go()
