import re

fichier="../../textes/formations.txt"

with open(fichier) as f:
    c = []
    for l in f.readlines():
        t = []
        if l.startswith('\t'):
            #ligne
            data = re.split(r'\t+', l)
            print(" Ligne : data=%s " % data )
        elif l == '<FF>\n':
            # Saut de page
            pass
        else:
            # Titre
            print("TITRE %s " % l)
