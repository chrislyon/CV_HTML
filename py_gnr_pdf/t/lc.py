import re

fichier="../../textes/competence.txt"

with open(fichier) as f:
    c = []
    for l in f.readlines():
        t = []
        if l.startswith('\t'):
            #ligne
            _, label, niveau, desc = re.split(r'\t+', l)
            print(" Ligne : l=%s n=%s d=%s" % (label, niveau, desc))
        elif l == '<FF>\n':
            # Saut de page
            pass
        else:
            # Titre
            print("TITRE %s " % l)
