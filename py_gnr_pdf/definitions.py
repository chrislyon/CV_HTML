## -------------------------------------
## DEFINITIONS DE LA BASE DE DONNEES
## -------------------------------------

import os
import sys
from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String, DateTime, Date, Float, BigInteger, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
import datetime

Base = declarative_base()

BASE_NAME = 'sqlite:///data.dbf'

class BASE_TABLE():
    " Classe commune a toute les autres"
    id = Column(Integer, primary_key=True)
    d_cre = Column(DateTime, default = datetime.datetime.now() )
    d_mod = Column(DateTime, default = datetime.datetime.now(), onupdate=datetime.datetime.now() )

class Contact(Base, BASE_TABLE):
    "Le contact"
    __tablename__ = 'CONTACT'
    Nom         = Column(String(30), nullable=False, unique=True)
    Adresse     = Column(String(30))
    DateNai     = Column(DateTime)
    Telephone   = Column(String(30))
    Email       = Column(String(250))
    Site        = Column(String(250))
    Titre       = Column(Text)

class Experiences(Base, BASE_TABLE):
    "Experiences"
    __tablename__ = 'EXPERIENCES'
    # Lien vers coordonnées
    contact_id  = Column(Integer, ForeignKey('CONTACT.id'))
    ordre       = Column(Integer)
    debut       = Column(String(30))
    fin         = Column(String(30))
    titre       = Column(String(30))
    localite    = Column(String(30))
    desc        = Column(String(100))

    #Competences
class Competences(Base, BASE_TABLE):
    "Competences"
    __tablename__ = 'COMPETENCES'
    # Lien vers coordonnées
    contact_id  = Column(Integer, ForeignKey('CONTACT.id'))
    titre       = Column(String(30))
    intitule    = Column(String(30))
    niveau      = Column(Integer)
    desc        = Column(String(100))

    #Formations 
class Formations(Base, BASE_TABLE):
    "Formations"
    __tablename__ = 'FORMATIONS'
    # Date 
    Date_form  = Column(String(30))
    Organisme  = Column(String(30))
    Desc       = Column(String(30))




    def __repr__(self):
        return "< contact : {0:} / {1:} >".format(self.Nom, self.Email)
# Création de la base de données
engine = create_engine(BASE_NAME)
 
# Création des tables dans la base de données
Base.metadata.create_all(engine)
