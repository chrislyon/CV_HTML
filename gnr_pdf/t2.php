<?php

// Experiences
function CV_Exp()
{
	$MARGE_GAUCHE = 15;
	$MARGE_HAUTE = 15;
	$LN = 5;

	// Ouvre le fichier et retourne un tableau contenant une ligne par élément
	$lines = file('../textes/competence.txt');
	/*On parcourt le tableau $lines et on affiche le contenu de chaque ligne précédée de son numéro*/
	foreach ($lines as $lineNumber => $lineContent)
	{
		$c = substr($lineContent,0,1);

		if ( ord($c) != 9 )
		{
			// Catégorie ou FF
			$t = trim( $lineContent);
			if ( $t != '<FF>')
			{
				printf("=>[%s]\n", $t);
			}
		}
		else
		{
			// Ligne
			$t = trim( $lineContent);
			$text = preg_split("/[\t]+/", $t);
			var_dump($text);
			printf("->[%s]\n", $t);
		}
	}

}

CV_Exp();

?>
