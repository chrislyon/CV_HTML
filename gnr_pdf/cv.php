<?php
require('fpdf181/fpdf.php');

                               

/*---------------------------------------------------------------*/
/*
    Titre : Calcul l'age àpartir d'une date de naissance                                                                 
                                                                                                                          
    URL   : https://phpsources.net/code_s.php?id=590
    Auteur           : anakink                                                                                            
    Date édition     : 08 Juin 2010                                                                                       
    Date mise à jour : 11 Aout 2019                                                                                      
    Rapport de la maj:                                                                                                    
    - fonctionnement du code vérifié                                                                                    
    - modification de la description                                                                                      
*/
/*---------------------------------------------------------------*/  

function age($date) 
{ 
	$age = date('Y') - $date; 
	if (date('md') < date('md', strtotime($date))) 
	{ 
		return $age - 1; 
	} 
		return $age; 
} 

function get_date()
{
	setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');
	return strftime("Généré le %A %d %B %Y à %H:%M"); //Affichera par exemple "date du jour en français : samedi 24 juin 2006."
}

// =================================
// CLASSE FPDF
//
//
// =================================
class PDF extends FPDF
{
	// En-tête
	function Header()
	{
		$this->SetFont('Arial', '', 8);
		$this->Cell(0, 0, 'CV Christophe BONNET', 0, 0, 'L');
		$horo = get_date();
		$this->Cell(0, 0, $horo, 0, 0, 'R');
		$this->Ln(20);
	}

	// Pied de page
	function Footer()
	{
		// Positionnement à 1,5 cm du bas
		$this->SetY(-15);
		// Police Arial italique 8
		$this->SetFont('Arial','I',8);
		// Numéro de page
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}

	function Cell($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='') 
	{
		parent::Cell($w,$h, utf8_decode($txt), $border,$ln,$align,$fill,$link);
	}
}

// ===============================
// Entete du CV
//
// ===============================
function Entete( $pdf )
{
	$MARGE_HAUTE = 30;
	$MARGE_GAUCHE = 10;
	$LN = 6;

	// Nom Adresse Age 
	$pdf->SetY($MARGE_HAUTE);
	$pdf->SetX($MARGE_GAUCHE);
	$pdf->SetFont('Arial', 'B', '14');
	$pdf->Cell(0,0, "Christophe BONNET");
	$pdf->Ln($LN);
	$pdf->SetFont('Arial', '', '11');
	$pdf->SetX($MARGE_GAUCHE);
	$pdf->Cell(0,0, "550 Route de Lagnieu");
	$pdf->Ln($LN);
	$pdf->SetX($MARGE_GAUCHE);
	$pdf->Cell(0,0, "01150 Blyes");
	$pdf->Ln($LN);
	$pdf->SetX($MARGE_GAUCHE);
	$pdf->Cell(0,0, sprintf("%s ans", age(1965) ));
	$pdf->Ln($LN);

	// Tel + email
	$pdf->SetY($MARGE_HAUTE);
	$pdf->SetFont('Arial', '', '11');
	$pdf->SetX($MARGE_GAUCHE+135);
	$pdf->SetFont('ZapfDingbats', '', '18');
	$pdf->Cell(0,0, chr(37));
	$pdf->SetX($MARGE_GAUCHE+145);
	$pdf->SetFont('Arial', '', '11');
	$pdf->Cell(0,0, " : 06 04 52 13 87");
	$pdf->Ln($LN);
	$pdf->SetX($MARGE_GAUCHE+135);
	$pdf->SetFont('ZapfDingbats', '', '18');
	$pdf->Cell(0,0, chr(41));
	$pdf->SetX($MARGE_GAUCHE+145);
	$pdf->SetFont('Arial', '', '11');
	$pdf->Cell(0,0, " : cb@cv.chrislyon.fr");
	$pdf->Ln($LN);
	$pdf->SetX($MARGE_GAUCHE+135);
	$pdf->SetFont('ZapfDingbats', '', '18');
	$pdf->Cell(0,0, chr(43));
	$pdf->SetX($MARGE_GAUCHE+145);
	$pdf->SetFont('Arial', '', '11');
	$pdf->Cell(0,0, " : https://cv.chrislyon.fr");
	$pdf->Ln($LN);

	$pdf->SetY($MARGE_HAUTE+40);
	$pdf->SetX($MARGE_GAUCHE);
	$pdf->SetFont('Arial', 'B', '16');
	$pdf->Cell(0,0, "Ingénieur Système UNIX , Linux, Certifié AIX", 0, 0, "C" );
	$pdf->Ln($LN);
	$pdf->Cell(0,0, "Administrateur Base de données Oracle", 0, 0, "C" );
	$pdf->Ln($LN);
	$pdf->Cell(0,0, "Développeur multi langage", 0, 0, "C" );
	$pdf->Ln($LN);
}

// Experiences
function CV_Exp( $pdf )
{
	$MARGE_HAUTE = 30;
	$MARGE_GAUCHE = 20;
	$LN = 5;

	$pdf->Ln($LN);
	$pdf->SetX($MARGE_GAUCHE);
	$pdf->SetFont('Arial', 'BU', '14');
	$pdf->Ln($LN);
	$pdf->Cell(0,0, "Expériences", 0, 0, "L" );
	$pdf->Ln($LN);

	// Ouvre le fichier et retourne un tableau contenant une ligne par élément
	$lines = file('../textes/experiences.txt');
	/*On parcourt le tableau $lines et on affiche le contenu de chaque ligne précédée de son numéro*/
	foreach ($lines as $lineNumber => $lineContent)
	{
		// Extraction des infos
		$t = substr( $lineContent, 0, 2);
		$r = substr( $lineContent, 2);
		switch ($t)
		{
		case 'h3':
			$pdf->Ln($LN);
			$pdf->Ln($LN);
			$pdf->SetX($MARGE_GAUCHE);
			$pdf->SetFont('Arial', 'B', '12');
			$pdf->Cell(0,0, $r, 0, 0, "L" );
			$pdf->Ln($LN);
			$pdf->Ln($LN);
			break;
		case 'li':
			$pdf->SetX($MARGE_GAUCHE+10);
			// Affichage DOT
			$pdf->SetFont('ZapfDingbats', '', '8');
			$pdf->Cell(0,0, chr(109));
			// Affichage Description
			$pdf->SetX($MARGE_GAUCHE+15);
			$pdf->SetFont('Arial', '', '10');
			$pdf->Cell(0,0, $r, 0, 0, "L" );
			$pdf->Ln($LN);
			break;
		default:
			break;
		}
	}

}

// Competences
//
function CV_notation_comp( $pdf)
{

	$MARGE_GAUCHE = 15;
	$MARGE_HAUTE = 30;
	//$pdf->SetY($MARGE_HAUTE+20);
	//$pdf->SetX($MARGE_GAUCHE);

	$ENT_NOT = "Sur ces pages vous trouverez le détail de mes compétences et pour ce faire j'ai utilisé la notification suivante :";
	$TAB_NOT = array (
		"Niveau" 		=> "Description du niveau",
		"Notions" 	 	=> "Connaissances des principes de bases, mais pas de mise en exploitation ou en production",
		"Utilisateur"	=> "Technologie testée et utilisée, pas ou très peu de mise en exploitation",
		"Confirmé" 		=> "Technologie avec maitrise des fondamentaux de l'administration, mise en oeuvre en production",
		"Maîtrise" 		=> "Technologie maîtrisée depuis de nombreuses années",
		"Expert" 		=> "Reconnu par les autres comme tel"
	);

	$LARG_NIV 	= 30;
	$LARG_NIV2 	= 10;

	$pdf->SetFont('Arial', '', '11');
	$pdf->Cell(0,6, $ENT_NOT, 0);
	$pdf->Ln();
	$pdf->Ln();

	$n = 0;

	foreach ($TAB_NOT as $key => $value) {
		if ( $key == "Niveau" )
		{
			$pdf->SetFont('Arial', 'B', '10');
			$pdf->Cell($LARG_NIV,6, $key, 1);
			$pdf->Cell($LARG_NIV2,6, " ", 1);
			$pdf->Cell(0,6, $value, 1);
			$pdf->Ln();
		}
		else
		{
			$pdf->SetFont('Arial', '', '10');
			$pdf->Cell($LARG_NIV,6, $key, 1);
			$n += 1;
			$pdf->Cell($LARG_NIV2,6, sprintf(" %d/5", $n), 1);
			$pdf->Cell(0,6, $value, 1);
			$pdf->Ln();
		}
	};
}

function CV_comp( $pdf )
{
	$MARGE_GAUCHE = 15;
	$MARGE_HAUTE = 30;
	$LN = 5;

	$pdf->SetY($MARGE_HAUTE+60);
	$pdf->SetX($MARGE_GAUCHE);
	$pdf->SetFont('Arial', 'BU', '14');
	$pdf->Cell(0,0, "Compétences", 0, 0, "L" );
	$pdf->Ln($LN);

	// Largeur des colonnes 
	$LARG_CATEG = 50;
	$LARG_DESC  = $LARG_CATEG;
	$LARG_NIV   = 10;
	$LARG_COM   = 0;

	// Ouvre le fichier et retourne un tableau contenant une ligne par élément
	$lines = file('../textes/competence.txt');

	foreach ($lines as $lineNumber => $lineContent)
	{
		$c = substr($lineContent,0,1);

		if ( ord($c) != 9 )
		{
			// Catégorie ou FF
			$t = trim($lineContent);
			switch ($t)
			{
			case '<FF>':
				break;
			case 'Virtualisation':
				$pdf->AddPage();
				$pdf->SetFont('Arial', 'B', '12');
				$pdf->Cell($LARG_CATEG,7, $t, 1);
				$pdf->Ln();
				break;
			default:
				$pdf->Ln();
				$pdf->SetFont('Arial', 'B', '12');
				$pdf->Cell($LARG_CATEG,7, $t, 1);
				$pdf->Ln();
				break;
			}
		}
		else
		{
			// Ligne
			$t = trim( $lineContent);
			$t = str_replace('<br/> ', '', $t);
			$t = str_replace('&#x1F609; ', '', $t);
			$text = preg_split("/[\t]+/", $t);
			$pdf->SetFont('Arial', '', '10');
			$pdf->Cell($LARG_DESC,6, "   ".$text[0], 1);
			$pdf->Cell($LARG_NIV,6, $text[1]." / 5", 1, 0, "C" );
			$pdf->MultiCell($LARG_COM,6, $text[2], 1);
			//$pdf->Ln();
			//var_dump($text);
			//printf("->[%s]\n", $t);
		}
	}
}

// Formations

function CV_Form( $pdf )
{
	$MARGE_GAUCHE = 15;
	$MARGE_HAUTE = 30;
	$LN = 5;

	//$pdf->SetY($MARGE_HAUTE+20);

	$pdf->SetX($MARGE_GAUCHE);
	$pdf->SetFont('Arial', 'BU', '14');
	$pdf->Cell(0,0, "Formations", 0, 0, "L" );
	$pdf->Ln($LN);

	// Largeur des colonnes 
	$LARG_CATEG = 60;
	$LARG_DESC  = $LARG_CATEG;
	$LARG_NIV   = 10;
	$LARG_COM   = 0;

	// Ouvre le fichier et retourne un tableau contenant une ligne par élément
	$lines = file('../textes/formations.txt');

	foreach ($lines as $lineNumber => $lineContent)
	{
		$c = substr($lineContent,0,1);

		if ( ord($c) != 9 )
		{
			// Catégorie ou FF
			$t = trim( $lineContent);
			$pdf->SetFont('Arial', 'B', '12');
			$pdf->Ln($LN);
			$pdf->Cell($LARG_CATEG,7, $t, 1, 0, "C");
			$pdf->Ln();
		}
		else
		{
			// Ligne
			$t = trim( $lineContent);
			$text = preg_split("/[\t]+/", $t);
			$pdf->SetFont('Arial', '', '10');
			$pdf->Cell($LARG_DESC,6, $text[0], 1, 0, "C");
			$pdf->Cell($LARG_COM,6, $text[1], 1);
			$pdf->Ln();
			//var_dump($text);
			//printf("->[%s]\n", $t);
		}
	}
}


// Publications

function CV_Publication( $pdf )
{
	$MARGE_GAUCHE = 15;
	$MARGE_HAUTE = 30;
	$LN = 5;

	$pdf->SetY($MARGE_HAUTE+130);

	$pdf->SetX($MARGE_GAUCHE);
	$pdf->SetFont('Arial', 'BU', '14');
	$pdf->Cell(0,0, "Publication", 0, 0, "L" );
	$pdf->Ln($LN);

	$l1 = 'Bonnet, Christophe. "Scripting Python sous Linux - Développez vos outils système". Editions ENI, Juin 2020'	;
	$l2 = 'https://www.editions-eni.fr/livre/scripting-python-sous-linux-developpez-vos-outils-systeme-9782409025679';

	$pdf->SetX($MARGE_GAUCHE+20);
	$pdf->Ln($LN);
	$pdf->SetFont('Arial', '', '11');
	$pdf->Cell(0,0, $l1);
	$pdf->Ln($LN);

	$image = "../images/EISCRYPT_lkn_552x288-publication.jpg";
	$curr_x = $pdf->GetX();
	$curr_y = $pdf->GetY();

	$pdf->Cell( 40, 40, $pdf->Image($image, $curr_x+40, $curr_y, 80), 0, 0, 'L', false );
	$pdf->SetX($MARGE_GAUCHE+20);
	$pdf->SetY($curr_y + 50);
	$pdf->Cell(0,0, $l2);

}

// ===========================
// Corps du CV 
// ===========================
function CV_Body( $pdf )
{
	Entete( $pdf );
	//body_test( $pdf );
	CV_Exp( $pdf );
	$pdf->AddPage();
	CV_notation_comp( $pdf );
	CV_comp( $pdf );
	$pdf->AddPage();
	CV_Form( $pdf );
	CV_Publication( $pdf );
}

// Instanciation de la classe dérivée
$pdf = new PDF();

$pdf->SetAuthor('Christophe BONNET');
$pdf->SetCreator('Script PHP Fpdf');
$pdf->SetSubject('CV Christophe BONNET');
$pdf->SetTitle('CV Christophe BONNET');
$pdf->SetKeywords('CV Christophe BONNET');
$pdf->SetDisplayMode('real');

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',12);
CV_Body( $pdf );
$pdf->Output('D', 'CV_CHRISTOPHE_BONNET.pdf');
?>
