#!  /usr/local/bin/python3.7

import pdb

class Vide():
    pass

def lire():
    FICHIER="textes/realisations.txt"

    data = []
    tab = None

    with open(FICHIER) as f:
        for l in f.readlines():
            l = l.rstrip()
            if l.startswith('#'):
                    continue
            elif l.startswith('\t'):
                tab.texte.append(l.lstrip())
            elif l == "":
                tab.texte.append(' ')
            else:
                if tab == None:
                    tab = Vide()
                    tab.TabTitre, tab.titre = l.split('|')
                    tab.texte = []
                else:
                    data.append(tab)
                    tab = Vide()
                    tab.TabTitre, tab.titre = l.split('|')
                    tab.texte = []

    data.append(tab)
    return data


def run():
    data = lire()
    titres = []
    tabtitres = []
    active = ""

    for l in data:
        tabtitres.append(l.TabTitre)
        titres.append(l.titre)

    n = 0
    ## La barre de nav pour les tabs
    print('ul(class="nav nav-tabs" id="myTab" role="tablist")')
    for l in data:
        n += 1
        ltab = 'tab%02d' % n
        if n == 1:
            active = "active"
        else:
            active=""
        print('\tli(class="nav-item")')
        print('\t\ta(class="nav-link %s bg-warning border border-dark" id="%s-tab" data-toggle="tab" href="#%s" role="tab" aria-controls="%s" aria-selected="true") %s' % (active, ltab, ltab, ltab, l.TabTitre))

    ## Les tabs
    n = 0
    print('div(class="tab-content" id="myTabContent")')
    for l in data:
        n += 1
        ltab = 'tab%02d' % n
        if n == 1:
            active = "active"
        else:
            active=""
        print('\tdiv(class="tab-pane fade show %s" id="%s" role="tabpanel" aria-labelledby="%s-tab")' % (active, ltab, ltab)) 
        print('\t\t<br>')
        print('\t\th3 %s' % l.titre )
        print('\t\t<br>')
        print('\t\tp(class="small") %s' % l.texte[0])
        for li in l.texte[1:]:
            print('\t\t\t| %s <br>' %li )

if __name__ == '__main__':
    run()
