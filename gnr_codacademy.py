#!  /usr/local/bin/python3.7

import csv

FIC = "textes/codacademy_badges.csv"

class Lig:
    def __init__(self, desc, date, cat, badge):
        self.desc = desc
        self.date = date
        self.cat  = cat
        self.badge = badge

def entete():
    r = """ 
//
// Table des competences
//
div(class="container")
    div(class="row")
        table(class="table table-bordered ")
            tbody
    """
    return r

def chapitre(c):
    r = """
            tr
                td(colspan=4 class="bg-info text-white")
                    h5 Catégorie %s
            tr
                th  &nbsp
                th  Description
                th  Date obtention
                th  Badge
    """ % c
    return r

def ligne(l):
    r = """ 
            tr
                td  &nbsp
                td  %s
                td  %s
                td 
                    img(src="/images/%s.png" width="30" height="30" alt=" Badge %s") 
                    
    """ % (l.desc, l.date, l.badge, l.badge)
    return r


DATA = []
CHAPITRE = [ 'PYTHON', 'JAVASCRIPT', 'HTML', 'CSS', 'GENERAL', 'OUTILS'  ]

## Lecture des donnees
with open(FIC) as csvfile:
    spamreader = csv.DictReader(csvfile, delimiter='|', quotechar='"')
    for row in spamreader:
        r = Lig(row['desc'], row['date'], row['cat'], row['badge'])
        DATA.append(r)


print(entete())
for c in CHAPITRE:
    print(chapitre(c))
    for r in DATA:
        if c == r.cat:
            print(ligne(r))

