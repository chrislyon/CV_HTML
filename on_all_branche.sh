
BRANCHES=$(git for-each-ref --format '%(refname:short)' refs/heads)

for b in $BRANCHES
do
	if [ "$b" != "master" ]
	then
		echo "Working on $b"
		git checkout $b
		git rm publish
		git commit -m "Suppression de publish"
	else
		echo "Skipping $b"
	fi
done
